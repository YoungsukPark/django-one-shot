from django.urls import path
from todos.views import (todo_list, show_list, create_list, edit_list,
delete_list, item_create, item_edit,)

urlpatterns = [
    path("", todo_list, name="todo_list"),
    path("<int:id>/", show_list, name="show_list"),
    path("create/", create_list, name="create_list"),
    path("<int:id>/edit/", edit_list, name="edit_list"),
    path("<int:id>/delete/", delete_list, name="delete_list"),
    path("item_create/", item_create, name="item_create"),
    path("<int:id>/item_edit/", item_edit, name="item_edit"),
]
